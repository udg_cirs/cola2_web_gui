// inner variables
var canvas, ctx;
var depth_array = []
var altitude_array = []
var total_array = []
var total_points = 50

// draw functions :
function clear() { // clear canvas function
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}

function drawDepth( depth, altitude ) { // main drawScene function
   
    var blue = '#0088CC';
    var red = '#BD362F';
    var green = '#51A351';
    var orange = '#F89406';
    var grey = '#808080';
    var black = '#003300';
    var brown = '#800000';

    canvas = document.getElementById('depth_canvas');
    ctx = canvas.getContext('2d');
    clear(); // clear canvas

    // save current context
    var width = canvas.width;
    var height = canvas.height;
    var right_padding = 40;

    // Save depth and altitude    
    depth_array.push(depth)    
    if(depth_array.length > total_points) {
        depth_array.splice(0, 1);
    }

    if (altitude < 0) altitude = 0.5

    altitude_array.push(altitude)    
    if(altitude_array.length > total_points) {
        altitude_array.splice(0, 1);
    }

    total_array.push(depth + altitude)    
    if(total_array.length > total_points) {
        total_array.splice(0, 1);
    }

    // Draw axis lines
    ctx.save();
    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.moveTo(right_padding, 20);
    ctx.lineTo(right_padding, height-10);
    ctx.lineTo(width, height-10);
    ctx.strokeStyle = black;
    ctx.stroke();
    ctx.restore();

    // Draw axis values
    ctx.font = ( Math.floor(Math.sqrt(Math.min(width, height)))) + 'px Arial';
    ctx.fillStyle = black;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    max_value = Math.max.apply(Math, total_array);
    inc_y = (height-30)/6;
    ratio = (height-30)/max_value
    for(var i = 0; i < 7; i++ ) {
        ctx.fillText(parseFloat(Math.round(max_value/6*i*10)/10).toFixed(1) + 'm', 18, inc_y*i+20);
        ctx.beginPath();
        if( i == 0 ) {
            ctx.lineWidth = 2;
            ctx.strokeStyle = blue;
        }
        else { 
            ctx.lineWidth = 1;
            ctx.strokeStyle = grey;
        }
        ctx.moveTo(right_padding, inc_y*i+20);
        ctx.lineTo(width, inc_y*i+20);
        ctx.stroke();
    }

    if( altitude < 2 ) {
        ctx.fillStyle = red;
    }
    else if( altitude < 5 ) {
        ctx.fillStyle = orange;
    }
    else {
        ctx.fillStyle = blue;
    }

    ctx.font = 'bolder ' + (Math.floor(Math.sqrt(Math.min(width, height)))*1.2) + 'px Arial';
    ctx.fillText('alt: ' + parseFloat(Math.round(altitude*10)/10).toFixed(1) + 'm', width - 50, 36);
    
    
    // Draw depth
    ctx.save();
    ctx.beginPath();
    inc_x = (width-right_padding)/total_points;
    ctx.moveTo(right_padding, depth_array[0]*ratio+20);
    for (var i = 1; i < depth_array.length; i++ ) {
        ctx.lineTo(right_padding+(inc_x*i), depth_array[i]*ratio + 20);
    }
    ctx.lineWidth = 2;
    ctx.strokeStyle = black;
    ctx.stroke();
    ctx.restore();

    // Draw total
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(right_padding, total_array[0]*ratio+20);
    for (var i = 1; i < total_array.length; i++ ) {
        ctx.lineTo(right_padding+(inc_x)*i, total_array[i]*ratio + 20);
    }
    ctx.lineWidth = 2;
    ctx.strokeStyle = brown;
    ctx.stroke();
    ctx.restore();
    
}

