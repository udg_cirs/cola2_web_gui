// inner variables
var canvas, ctx;
var clockImage;

// draw functions :
function clear() { // clear canvas function
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}

function drawTimeout(time, timeout ) { // main drawScene function
    var blue = '#0088CC';
    var red = '#BD362F';
    var green = '#51A351';
    var orange = '#F89406';
    var grey = '#808080';
    var black = '#003300';
    var brown = '#800000';

    canvas = document.getElementById('timeout_canvas');
    ctx = canvas.getContext('2d');
    clear(); // clear canvas

    // save current context
    var width = canvas.width;
    var height = canvas.height;
    var compass_radius = Math.min(width, height)/2.1;

    ctx.save();
    
    // draw circle (as background)
    ctx.beginPath();
    ctx.arc(width/2, height/2, Math.min(width, height)/3, 0, 2 * Math.PI, false);
    ctx.lineWidth = Math.min(width, height)/20;
    ctx.strokeStyle = black;
    ctx.stroke();

    var charge = (1 - (time/timeout) ) * 100.0;
    if ( time > timeout ) charge = 0.0;

    ctx.beginPath();
    ctx.arc(width/2, height/2, Math.min(width, height)/3, -1.57, Math.round(charge*10)/1000 * 2 * Math.PI -1.57, false);
    ctx.lineWidth = Math.min(width, height)/10;
    if (charge > 20)
        ctx.strokeStyle = green;
    else if (charge > 10)
        ctx.strokeStyle = orange;
    else
        ctx.strokeStyle = red;
    ctx.stroke();
    

    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.beginPath();

    // draw numbers
    ctx.save();
    //ctx.rotate(-angle*Math.PI/180);
    ctx.font = 'bolder ' + ( Math.floor(Math.sqrt(Math.min(width, height)) * 1.7 ) ) + 'px Arial';

    if (charge <= 10)
        ctx.fillStyle = red;
    else 
        ctx.fillStyle = '#000';

    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    
    // Draw %
    ctx.fillText(timeout-time, 0, 5);
    ctx.restore()
    ctx.restore();
}

