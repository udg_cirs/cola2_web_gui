// inner variables
var canvas, ctx;
var clockImage;

// draw functions :
function clear() { // clear canvas function
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}

function drawCompass( angle ) { // main drawScene function
   
    var blue = '#0088CC';
    var red = '#BD362F';
    var green = '#51A351';
    var orange = '#F89406';
    var grey = '#808080';
    var black = '#003300';
    var brown = '#800000';

    canvas = document.getElementById('compass_canvas');
    ctx = canvas.getContext('2d');
    clear(); // clear canvas

    // save current context
    var width = canvas.width;
    var height = canvas.height;
    var compass_radius = Math.min(width, height)/2.1;

    ctx.save();
    var angle = angle;
    
    // draw clock image (as background)
    // ctx.drawImage(clockImage, 0, 0, 500, 500);
    ctx.beginPath();
    ctx.arc(width/2, height/2, Math.min(width, height)/2.1, 0, 2 * Math.PI, false);
    ctx.lineWidth = 3;
    ctx.strokeStyle = black;
    ctx.stroke();


    ctx.translate(width/2, height/2);
    ctx.beginPath();

    // draw numbers
    ctx.save();
    ctx.font = ( Math.floor(Math.sqrt(Math.min(width, height))) + 2 ) + 'px Arial';
    ctx.fillStyle = black;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    var compass_values = ['30º', '60º', 'E', '120º', '150º', 'S', '210º', '240º', 'W', '300º', '330º', 'N']
    for (var n = 1; n <= 12; n++) {
        var theta = ((n - 3) * (Math.PI * 2) / 12) - angle/180.0*Math.PI;
        var x = compass_radius * 0.8 * Math.cos(theta);
        var y = compass_radius * 0.8 * Math.sin(theta);
        if(n == 3 || n == 6 || n == 9 || n == 12 ) 
            ctx.font = 'bolder ' + ( Math.floor(Math.sqrt(Math.min(width, height))) * 1.8 ) + 'px Arial';
        else 
            ctx.font = ( Math.floor(Math.sqrt(Math.min(width, height))) + 2 ) + 'px Arial';
        ctx.fillText(compass_values[n-1], x, y);
    }
    ctx.restore()

    // draw angle
    ctx.save();
    var theta = -Math.PI/2;
    ctx.rotate(theta);
    ctx.beginPath();
    ctx.moveTo(-15, -3);
    ctx.lineTo(-15, 3);
    ctx.lineTo(compass_radius * 0.95, 1);
    ctx.lineTo(compass_radius * 0.95, -1);
    ctx.fillStyle = red;
    ctx.fill();
    ctx.restore();
    ctx.restore();

    // Draw angle
    ctx.font = 'bolder ' + ( Math.floor(Math.sqrt(Math.min(width, height))) + 2 ) + 'px Arial';
    ctx.fillStyle = black;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.fillStyle = blue;
    if( angle < 0 ) angle = 360 + angle;
    ctx.fillText(parseFloat(Math.round(angle*10)/10).toFixed(1) + 'º', width/2, 3*height/4-10);
    ctx.restore()
}

