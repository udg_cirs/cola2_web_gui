function drawSetpoints( setpoints ) { // main drawScene function
   
    // console.log('draw setpoint');
    var blue = '#0088CC';
    var red = '#BD362F';
    var green = '#51A351';
    var orange = '#F89406';
    var grey = '#808080';
    var black = '#003300';
    var brown = '#800000';

    canvas = document.getElementById('setpoints_canvas');
    ctx = canvas.getContext('2d');

    // save current context
    var width = canvas.width;
    var height = canvas.height;
    var n_setpoints = setpoints.length;
    var bar_width = Math.round( (canvas.width / n_setpoints) - 10 );

    // console.log('bar_width: ' + bar_width + ', num thrusters: ' + n_setpoints);
    //Clear Canvas
    ctx.clearRect(0, 0, width, height);
    
    for (i = 0; i <  n_setpoints; i++) {
        // Choose color
        if( setpoints[i] > 0) 
            ctx.fillStyle = green;
        else 
            ctx.fillStyle = red;
        // Draw rectangle
        ctx.fillRect( (bar_width + 10)* i, height/2, bar_width, -1 * setpoints[i]*height/2 + 0.05);
        ctx.stroke();

        // Draw Value
        ctx.font = 'bolder ' + ( Math.floor(Math.sqrt(Math.min(width, height))) + 2 ) + 'px Arial';
        ctx.fillStyle = black;
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillStyle = black;
        ctx.fillText(parseFloat(Math.round(setpoints[i]*100)) + '%', (bar_width + 10) *i + bar_width/2, -1*setpoints[i]*height/4 + height/2);
    }
}
