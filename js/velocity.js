// inner variables
var canvas, ctx;
var clockImage;

// draw functions :
function clear() { // clear canvas function
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}

function drawVelocity( velocity ) { // main drawScene function

    var blue = '#0088CC';
    var red = '#BD362F';
    var green = '#51A351';
    var orange = '#F89406';
    var grey = '#808080';
    var black = '#003300';
    var brown = '#800000';
    var light_red = '#E77471';

    canvas = document.getElementById('velocity_canvas');
    ctx = canvas.getContext('2d');
    clear(); // clear canvas

    // save current context
    var width = canvas.width;
    var height = canvas.height;
    var compass_radius = Math.min(width, height)/2.1;

    ctx.save();
    
    // draw circle (as background)
    ctx.beginPath();
    ctx.arc(width/2, height/2, Math.min(width, height)/2.1, Math.PI/2, Math.PI, false);
    ctx.lineTo(width/2, height/2)
    ctx.closePath();
    ctx.fillStyle = light_red;
    ctx.fill();


    ctx.beginPath();
    ctx.arc(width/2, height/2, Math.min(width, height)/2.1, 0, 2 * Math.PI, false);

    ctx.lineWidth = 3;
    ctx.strokeStyle = black;
    ctx.stroke();
    
    ctx.translate(width/2, height/2);
    ctx.beginPath();

    // draw numbers
    ctx.save();
    //ctx.rotate(-angle*Math.PI/180);
    ctx.font = ( Math.floor(Math.sqrt(Math.min(width, height))) + 2 ) + 'px Arial';
    ctx.fillStyle = black;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    var speed_values = ['0.8', '1.0', '1.2', '1.4', '1.6', '1.8', '-0.4', '-0.2', '0.0', '0.2', '0.4', '0.6']
    for (var n = 1; n <= 12; n++) {
        var theta = (n - 3) * (Math.PI * 2) / 12;
        var x = compass_radius * 0.8 * Math.cos(theta);
        var y = compass_radius * 0.8 * Math.sin(theta);
        ctx.fillText(speed_values[n-1], x, y);
    }
    // Draw speed
    ctx.font = 'bolder ' + ( Math.floor(Math.sqrt(Math.min(width, height))) + 2 ) + 'px Arial';
    ctx.fillStyle = blue;
    ctx.fillText(parseFloat(Math.round(velocity*100)/100).toFixed(2) + 'm/s', 0, height/4-10);
    ctx.restore()

    // draw angle
    if (velocity > 1.8) velocity = 1.8;
    if (velocity < -0.5 ) velocity = -0.5;
    ctx.save();
    var ratio = (Math.PI/2)/0.6 ;
    var theta = (velocity * ratio) - Math.PI;
    //var theta = -1.57    
    ctx.rotate(theta);
    ctx.beginPath();
    ctx.moveTo(-15, -3);
    ctx.lineTo(-15, 3);
    ctx.lineTo(compass_radius * 0.95, 1);
    ctx.lineTo(compass_radius * 0.95, -1);
    ctx.fillStyle = red;
    ctx.fill();
    ctx.restore();
    ctx.restore();
}

