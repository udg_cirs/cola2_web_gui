function rad2deg( rad ) {
    return rad * 180.0 / Math.PI;
};


//Subscribing to nav_sts topic
//----------------------
var listener = new ROSLIB.Topic({
    ros : ros,
    name : '/' + document.getElementById('namespace').value + '/navigator/navigation_throttle',
    messageType : 'cola2_msgs/NavSts'
});

// Add a callback to be called every time a message is published on this topic.
listener.subscribe(function(message) {
    var pose = [ document.getElementById("pose_x"), 
                 document.getElementById("pose_y"), 
                 document.getElementById("pose_z"), 
                 document.getElementById("pose_roll"), 
                 document.getElementById("pose_pitch"), 
                 document.getElementById("pose_yaw") ];

    pose[0].innerHTML = parseFloat(Math.round(message.position.north*100)/100).toFixed(2);
    pose[1].innerHTML = parseFloat(Math.round(message.position.east*100)/100).toFixed(2);
    pose[2].innerHTML = parseFloat(Math.round(message.position.depth*100)/100).toFixed(2);
    pose[3].innerHTML = parseFloat(Math.round(rad2deg(message.orientation.roll)*100)/100).toFixed(2);
    pose[4].innerHTML = parseFloat(Math.round(rad2deg(message.orientation.pitch)*100)/100).toFixed(2);
    var yaw = rad2deg(message.orientation.yaw);
    if( yaw < 0.0 ) yaw = yaw + 360.0;
    pose[5].innerHTML = parseFloat(Math.round(yaw*100)/100).toFixed(2);

    var twist = [ document.getElementById("twist_x"), 
                  document.getElementById("twist_y"), 
                  document.getElementById("twist_z"), 
                  document.getElementById("twist_roll"), 
                  document.getElementById("twist_pitch"), 
                  document.getElementById("twist_yaw") ];

    twist[0].innerHTML = parseFloat(Math.round(message.body_velocity.x*100)/100).toFixed(2);
    twist[1].innerHTML = parseFloat(Math.round(message.body_velocity.y*100)/100).toFixed(2);
    twist[2].innerHTML = parseFloat(Math.round(message.body_velocity.z*100)/100).toFixed(2);
    twist[3].innerHTML = parseFloat(Math.round(rad2deg(message.orientation_rate.roll)*100)/100).toFixed(2);
    twist[4].innerHTML = parseFloat(Math.round(rad2deg(message.orientation_rate.pitch)*100)/100).toFixed(2);
    twist[5].innerHTML = parseFloat(Math.round(rad2deg(message.orientation_rate.yaw)*100)/100).toFixed(2);
});

//Subscribing to merged_world_waypoint_req topic
//----------------------
var listenerWWR = new ROSLIB.Topic({
    ros : ros,
    name : '/' + document.getElementById('namespace').value + '/controller/merged_world_waypoint_req_throttle',
    messageType : 'cola2_msgs/WorldWaypointReq'
});

// Add a callback to be called every time a message is published on this topic.
listenerWWR.subscribe(function(message) {
    var wwr_value = [ document.getElementById("d_pose_x"), 
                      document.getElementById("d_pose_y"), 
                      document.getElementById("d_pose_z"), 
                      document.getElementById("d_pose_roll"), 
                      document.getElementById("d_pose_pitch"), 
                      document.getElementById("d_pose_yaw") ];
    if (message.disable_axis.x)
        wwr_value[0].style.color = "#AAAAAA";
    else 
        wwr_value[0].style.color = "#000000";
    if (message.disable_axis.y)
        wwr_value[1].style.color = "#AAAAAA";
    else 
        wwr_value[1].style.color = "#000000";
    if (message.disable_axis.z)
        wwr_value[2].style.color = "#AAAAAA";
    else 
        wwr_value[2].style.color = "#000000";
    if (message.disable_axis.roll)
        wwr_value[3].style.color = "#AAAAAA";
    else 
        wwr_value[3].style.color = "#000000";
    if (message.disable_axis.pitch)
        wwr_value[4].style.color = "#AAAAAA";
    else 
        wwr_value[4].style.color = "#000000";
    if (message.disable_axis.yaw)
        wwr_value[5].style.color = "#AAAAAA";
    else 
        wwr_value[5].style.color = "#000000";

    wwr_value[0].innerHTML = parseFloat(Math.round(message.position.north*100)/100).toFixed(2);
    wwr_value[1].innerHTML = parseFloat(Math.round(message.position.east*100)/100).toFixed(2);
    wwr_value[2].innerHTML = parseFloat(Math.round(message.position.depth*100)/100).toFixed(2);
    wwr_value[3].innerHTML = parseFloat(Math.round(rad2deg(message.orientation.roll)*100)/100).toFixed(2);
    wwr_value[4].innerHTML = parseFloat(Math.round(rad2deg(message.orientation.pitch)*100)/100).toFixed(2);
    var yaw = rad2deg(message.orientation.yaw);
    if( yaw < 0.0 ) yaw = yaw + 360.0;
    wwr_value[5].innerHTML = parseFloat(Math.round(yaw*100)/100).toFixed(2);
});

//Subscribing to merged_body_velocity_req topic
//----------------------
var listenerBVR = new ROSLIB.Topic({
    ros : ros,
    name : '/' + document.getElementById('namespace').value + '/controller/merged_body_velocity_req_throttle',
    messageType : 'cola2_msgs/BodyVelocityReq'
});

// Add a callback to be called every time a message is published on this topic.
listenerBVR.subscribe(function(message) {
    var bvr_value = [ document.getElementById("d_twist_x"), 
                      document.getElementById("d_twist_y"), 
                      document.getElementById("d_twist_z"), 
                      document.getElementById("d_twist_roll"), 
                      document.getElementById("d_twist_pitch"), 
                      document.getElementById("d_twist_yaw") ];
    if (message.disable_axis.x)
        bvr_value[0].style.color = "#AAAAAA";
    else 
        bvr_value[0].style.color = "#000000";
    if (message.disable_axis.y)
        bvr_value[1].style.color = "#AAAAAA";
    else 
        bvr_value[1].style.color = "#000000";
    if (message.disable_axis.z)
        bvr_value[2].style.color = "#AAAAAA";
    else 
        bvr_value[2].style.color = "#000000";
    if (message.disable_axis.roll)
        bvr_value[3].style.color = "#AAAAAA";
    else 
        bvr_value[3].style.color = "#000000";
    if (message.disable_axis.pitch)
        bvr_value[4].style.color = "#AAAAAA";
    else 
        bvr_value[4].style.color = "#000000";
    if (message.disable_axis.yaw)
        bvr_value[5].style.color = "#AAAAAA";
    else 
        bvr_value[5].style.color = "#000000";

    bvr_value[0].innerHTML = parseFloat(Math.round(message.twist.linear.x*100)/100).toFixed(2);
    bvr_value[1].innerHTML = parseFloat(Math.round(message.twist.linear.y*100)/100).toFixed(2);
    bvr_value[2].innerHTML = parseFloat(Math.round(message.twist.linear.z*100)/100).toFixed(2);
    bvr_value[3].innerHTML = parseFloat(Math.round(rad2deg(message.twist.angular.x)*100)/100).toFixed(2);
    bvr_value[4].innerHTML = parseFloat(Math.round(rad2deg(message.twist.angular.y)*100)/100).toFixed(2);
    bvr_value[5].innerHTML = parseFloat(Math.round(rad2deg(message.twist.angular.z)*100)/100).toFixed(2);
});
