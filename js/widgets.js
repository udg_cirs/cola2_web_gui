var blue = '#0088CC';
var red = '#BD362F';
var green = '#51A351';
var orange = '#F89406';
var grey = '#808080';
var black = '#003300';

var table_width = 0;

// Drop down menu
// function connect_server() {
//     var e = document.getElementById("vehicle");
//     console.log("VEHICLE: " + e.options[e.selectedIndex].value)
//     var strUser = e.options[e.selectedIndex].value;
//     //window.location.href = 'index.html?server=' + strUser;
// }

// Define canvas width and height
function resizeWidgets() {
    tmp = parseFloat(document.getElementById('table_widgets').offsetWidth);
    if( tmp != table_width ) {
        table_width = tmp;
        document.getElementById('compass_canvas').width = +Math.round(table_width/3.4);
        document.getElementById('compass_canvas').height = +Math.round(table_width/3.4);
        document.getElementById('horizon_canvas').width = +Math.round(table_width/3.4);
        document.getElementById('horizon_canvas').height = +Math.round(table_width/3.4);
        document.getElementById('velocity_canvas').width = +Math.round(table_width/3.4);
        document.getElementById('velocity_canvas').height = +Math.round(table_width/3.4);
        document.getElementById('depth_canvas').width = +Math.round(2*table_width/3.4);
        document.getElementById('depth_canvas').height = +Math.round(table_width/3.4);
        document.getElementById('charge_canvas').width = +Math.round(table_width/5.5);
        document.getElementById('charge_canvas').height = +Math.round(table_width/5.5);
        document.getElementById('timeout_canvas').width = +Math.round(table_width/5.5);
        document.getElementById('timeout_canvas').height = +Math.round(table_width/5.5);
        document.getElementById('setpoints_canvas').width = +Math.round(table_width);
        document.getElementById('setpoints_canvas').height = +Math.round(table_width/3.4);
    }
}
var interval_resize = setInterval(function(){resizeWidgets();}, 200);

// Connecting to ROS
// -----------------
var ros = new ROSLIB.Ros();


// If there is an error on the backend, an 'error' emit will be emitted.
ros.on('error', function(error) {
    console.log(error);
});

// Create a connection to the rosbridge WebSocket server.
function getParam(name)
{   var input_params = window.location.search.replace('?', '');
    var params = input_params.split('&');
    for(i=0; i < params.length; ++i)
    {
        key_value = params[i].split('=');
        if (key_value[0] == name)
        {
            return key_value[1];
        }
    }
    return "";
}

var server_name = 'ws://'+ getParam('server_ip') +':' + getParam('server_port');
console.log("connect to server: " + server_name );
ros.connect(server_name);
document.getElementById('server_ip').value = getParam('server_ip');
document.getElementById('server_port').value = getParam('server_port');
document.getElementById('namespace').value = getParam('namespace');

// NAVIGATION
var listener = new ROSLIB.Topic({
    ros : ros,
    name : '/' + document.getElementById('namespace').value + '/navigator/navigation_throttle',
    messageType : 'cola2_msgs/NavSts'
});

listener.subscribe(function(message) {
    drawCompass(message.orientation.yaw*180/3.14159);
    drawHorizon(message.orientation.roll*180/3.14159,
                message.orientation.pitch*180/3.14159)
    drawDepth(message.position.depth,
              message.altitude);
    drawVelocity(message.body_velocity.x);
});

// THRUSTERS DATA
var listenerSP = new ROSLIB.Topic({
    ros : ros,
    name : '/' + document.getElementById('namespace').value + '/controller/thruster_setpoints_throttle',
    messageType : 'cola2_msgs/Setpoints'
});

listenerSP.subscribe( function(message) {
    drawSetpoints( message.setpoints );
});


// MISSION TIMEOUT
var total_timeout = 3600;

var timeout = new ROSLIB.Param({
    ros : ros,
    name : '/' + document.getElementById('namespace').value + '/safety/timeout'
});

var listenerUpTime = new ROSLIB.Topic({
    ros : ros,
    name : '/' + document.getElementById('namespace').value + '/cola2_watchdog/elapsed_time',
    messageType : 'std_msgs/Int32'
});

listenerUpTime.subscribe( function(message) {
    timeout.get(function(value) {
        total_timeout = value;
    } );

    drawTimeout( message.data,  total_timeout);
});


// DIAGNOSTICS
var listener_diagnostic = new ROSLIB.Topic({
    ros : ros,
    name : '/' + document.getElementById('namespace').value + '/diagnostics_agg',
    messageType : 'diagnostic_msgs/DiagnosticArray'
});

listener_diagnostic.subscribe(function(message) {

    // Draw battery Charge from diagnostics
    for( var i = 0; i < message.status.length; i++ ) {
        if( message.status[i].name == '/safety/ battery' ) {
            for( var j = 0; j < message.status[i].values.length; j++ ) {
                if (message.status[i].values[j].key == 'charge') {
                    drawCharge(parseFloat(message.status[i].values[j].value));
                }
            }
        }
    }

    // General check for diagnostics
    var status = 0
    var errors = '';
    for(var i = 0; i < message.status.length; i++) {

        if (status < 2 && message.status[i].level == 1) {
            status = 1;
            errors = errors + ' ' + message.status[i].message;
        }
        if (message.status[i].level == 2) {
            errors = errors + ' ' + message.status[i].message;
            status = 2;
        }

        if ( status == 0 )  {
            document.getElementById("diagnostic_message").style.background = green;
            document.getElementById("diagnostic_message").innerHTML = 'Diagnostics Ok';
        }
        else if ( status == 1 ) {
            document.getElementById("diagnostic_message").style.background = orange;
            document.getElementById("diagnostic_message").innerHTML = errors;
        }
        else {
            document.getElementById("diagnostic_message").style.background = red;
            document.getElementById("diagnostic_message").innerHTML = errors;
        }

    }
});

  // Calling services
  // -----------------

  // Enable GOTO
  var enable_goto = new ROSLIB.Service({
    ros : ros,
    name : '/' + document.getElementById('namespace').value + '/captain/enable_goto',
    serviceType : 'cola2_msgs/Goto'
  });

 //  // Digital outputs
 //  var digital_output_srv = new ROSLIB.Service({
 //    ros : ros,
 //    name : '/girona500/digital_output',
 //    serviceType : 'cola2_msgs/DigitalOutput'
 //  });
 //
 // var lightsOn = new ROSLIB.ServiceRequest({
 //    digital_out : 13,
 //    value : true
 //  });
 //
 // var lightsOff = new ROSLIB.ServiceRequest({
 //    digital_out : 13,
 //    value : false
 //  });


//FUNCTIONS
function callEnableGoto() {
    var _x = parseFloat(document.getElementById("position_x").value);
    var _y = parseFloat(document.getElementById("position_y").value);
    var _z = parseFloat(document.getElementById("position_z").value);
    var _altitude_mode = document.getElementById("altitude_mode").checked;
    var _reference = parseInt(document.getElementById("reference").value);
    var _tx = parseFloat(document.getElementById("tolerance_x").value);
    var _ty = parseFloat(document.getElementById("tolerance_y").value);
    var _tz = parseFloat(document.getElementById("tolerance_z").value);
    var _speed = parseFloat(document.getElementById("speed").value);

    console.log("Call service enable_goto " + _x + ", " + _y + ", " + _z + " mode: " + _altitude_mode + ", reference: " + _reference);
    var params = new ROSLIB.ServiceRequest({
       position: {
           x: _x,
           y: _y,
           z: _z
       },
       yaw: 0.0,
       altitude: _z,
       altitude_mode: _altitude_mode,
       blocking: false,
       keep_position: false,
       priority: 10,
       reference: _reference,
       disable_axis: {
           x: false,
           y: true,
           z: false,
           roll: true,
           pitch: true,
           yaw: false
       },
       position_tolerance: {
           x: _tx,
           y: _tx,
           z: _tz
       },
       orientation_tolerance: {
           roll: 0.5,
           pitch: 0.5,
           yaw: 0.5
       },
       linear_velocity: {
           x: _speed,
           y: 0.0,
           z: 0.4
       },
       angular_velocity: {
           roll: 0.0,
           pitch: 0.0,
           yaw: 0.3
       },
       timeout: 0
   });
   enable_goto.callService(params, function(result){console.log(result);});
}


// function callLightsOn() {
//     console.log( "Call lights on service: " + digital_output_srv.name + " --> " + lightsOn );
//     digital_output_srv.callService(lightsOn, function(result) { console.log( result.success ); })
// }
//
// function callLightsOff() {
//     console.log( "Call lights off service: " + digital_output_srv.name + " --> " + lightsOff );
//     digital_output_srv.callService(lightsOff, function(result) { console.log( result.success ); })
// }
